#include <iostream>
#include <ctime>

#define FULL_MASK 0xffffffff
#define gpuErrorCheck(ans) { gpuAssert((ans), __FILE__, __LINE__); }

//Error check wrapper for cuda function calls
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
    if (code != cudaSuccess)
    {
        fprintf(stderr,"GPU assert: %s %s %d\n", cudaGetErrorString(code), file, line);
        if (abort) exit(code);
    }
}

void random_number_initializer(float *, int, bool = false);

void fast_wrapper(float*, float*, float*, int);
void fast_wrapper_v2(float*, float*, float*, int);

void efficient_wrapper(float *, float *, float *, int);

//The function takes d_a in row major and d_b in column major to improve upon spacial memory dependence.
//d_c is N^3 in size to accommodate all the partial products.
__global__ void multiply_kernel(int N, const float *d_a, const float *d_b, float *d_c) {
    unsigned int gid = blockIdx.x * blockDim.x + threadIdx.x;

    //These are matrix dimensions
    // A = p x q
    // B = q x r
    unsigned int p = N, q = N, r = N;

    unsigned int i = int(gid / (q * r)) * q + (gid % q);
    unsigned int j = gid % (q * r);

    if(gid < p * q * r) {
        //Multiply
        d_c[gid] = d_a[i] * d_b[j];
//        printf("gid : %d in with a = %d, b = %d and c = %d\n", gid,d_a[i], d_b[j], d_c[gid]);
    }
}

__global__ void add_kernel(float *a, float *b, float *c, int N, bool flip) {
    unsigned int gid = blockIdx.x * blockDim.x + threadIdx.x;

    if(gid < N * N) {
        if(flip) {
            int i = gid / N;
            int j = gid % N;
            c[gid] = a[gid] + b[j * N + i];
        }
        else {
            c[gid] = a[gid] + b[gid];
        }
    }
}

__global__ void mul_add_kernel(int N, const float *d_c, float *result_c) {
    //Function separated to reduce the spacial memory dependence of threads.
    unsigned int gid = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int rangeStart = gid * N;

    if(gid < N * N) {
        //Add
        for(int k = 0; k < N; k++) {
            result_c[gid] += d_c[rangeStart + k];
        }
    }
}

__global__ void mul_add_kernel_ver2(int N, const float *d_c, float *result_c) {
    //Function separated to reduce the spacial memory dependence of threads.
    unsigned int gid = blockIdx.x * blockDim.x + threadIdx.x;

    if(gid < N * N * 32) {
        //Consecutive 32 threads fall into the same warp. Because the thread block is a multiple of 32.
        int warp_id = gid / 32;
        int local_id = gid % 32; // Id of a thread within a warp.
        int load_per_thread = N / 32;

        unsigned int rangeStart = warp_id * N;
        unsigned int warp_offset = local_id * load_per_thread + rangeStart;

        float sum = 0;

        #pragma unroll
        for(int k = 0; k < load_per_thread; k++) {
            sum += d_c[warp_offset + k];
        }

        //Accumulate sum values for a warp.
        #pragma unroll
        for (int offset = 16; offset > 0; offset /= 2) {
            sum += __shfl_down_sync(FULL_MASK, sum, offset);
        }

        //First thread of a warp now has the actual sum.
        if(local_id == 0) {
            result_c[warp_id] += sum;
        }
    }
}

__global__ void random_number_gen(const int N, float *d_c, bool isZero) {
    unsigned int gid = blockDim.x * blockIdx.x + threadIdx.x;
    if(gid < N * N) {
        if (isZero) {
            d_c[gid] = 0;
        } else {
            d_c[gid] = gid % 15;
        }
    }
}

__global__ void efficient_multiply(float *a, float *b, float *c, int N) {
    int gid = blockIdx.x * blockDim.x + threadIdx.x;

    //Consecutive 32 threads fall into the same warp. Because the thread block is a multiple of 32.
    int warp_id = gid / 32;
    int local_id = gid % 32; // Id of a thread within a warp.

    //Each warp is dedicated for one element of c matrix.
    if(warp_id < N * N) {
        float sum = 0;
        int rowOffset = warp_id / N;
        int columnOffset = warp_id % N;

        int load_per_thread = N / 32; //This defines number of elements one thread has to deal with.
        int offset_for_thread = local_id * load_per_thread;
        rowOffset = rowOffset * N + offset_for_thread;
        columnOffset = columnOffset * N + offset_for_thread;

        #pragma unroll
        for(int i = 0; i < 256; i++) {
            sum += a[rowOffset + i] * b[columnOffset + i];
        }

        //Accumulate sum values for a warp.
        #pragma unroll
        for (int offset = 16; offset > 0; offset /= 2) {
            sum += __shfl_down_sync(FULL_MASK, sum, offset);
        }

        //First thread of a warp now has the actual sum.
        if(local_id == 0) {
            c[warp_id] += sum;
        }
    }
}

void print_mat(float *a, int N) {
    printf("---------------------------\n");

    for(int i = 0; i < N; i++) {
        for(int j = 0; j < N; j++) {
            printf("%f  ", a[i * N + j]);
        }
        printf("\n");
    }

    printf("---------------------------\n");
}

int main(int argc, char** argv) {
    int N = 16800;

    if(argc > 1) {
        N = strtol(argv[1], nullptr, 10);
    }

    float *a, *b, *c;

    unsigned int inputSize = N * N * sizeof(float);
    a = (float *)malloc(inputSize);
    b = (float *)malloc(inputSize);
    c = (float *)malloc(inputSize);

    printf("Starting number initialization\n");
    random_number_initializer(a, N);
    random_number_initializer(b, N);
    random_number_initializer(c, N, true);

    printf("Starting fast\n");
    time_t fast_start_v2 = clock();
    fast_wrapper_v2(a, b, c, N);
    time_t fast_end_v2 = clock();

    printf("time : %lld", (fast_end_v2 - fast_start_v2));


    free(a);
    free(b);
    free(c);

//    a = (float *)malloc(inputSize);
//    b = (float *)malloc(inputSize);
//    c = (float *)malloc(inputSize);
//
//    printf("Starting number initialization\n");
//    random_number_initializer(a, N);
//    random_number_initializer(b, N);
//    random_number_initializer(c, N, true);
//
//    printf("Starting fast\n");
//    fast_start_v2 = clock();
//    fast_wrapper(a, b, c, N);
//    fast_end_v2 = clock();
//
//
//    free(a);
//    free(b);
//    free(c);
//
//    a = (float *)malloc(inputSize);
//    b = (float *)malloc(inputSize);
//    c = (float *)malloc(inputSize);
//
//    printf("Starting number initialization\n");
//    random_number_initializer(a, N);
//    random_number_initializer(b, N);
//    random_number_initializer(c, N, true);
//
//    printf("Starting fast\n");
//    time_t fast_start = clock();
//    fast_wrapper_v2(a, b, c, N);
//    time_t fast_end = clock();
//
//    free(a);
//    free(b);
//    free(c);
//
//    printf("Fast time 2 : %lld v/s Fast time 1 : %lld\n",
//           (fast_end - fast_start),
//           (fast_end_v2 - fast_start_v2)
//    );

//    N = 16384;
//    inputSize = N * N * sizeof(float);
//    a = (float *)malloc(inputSize);
//    b = (float *)malloc(inputSize);
//    c = (float *)malloc(inputSize);
//
//    printf("Starting number initialization\n");
//    random_number_initializer(a, N);
//    random_number_initializer(b, N);
//    random_number_initializer(c, N, true);
//
//    printf("Starting efficient\n");
//    time_t efficient_start = clock();
//    efficient_wrapper(a, b, c, N);
//    time_t efficient_end = clock();
//
//    printf("Efficient Time : %lld\n", (efficient_end - efficient_start));
//
//    printf("Fast time : %lld v/s Efficient time : %lld\n",
//           (fast_end - fast_start),
//           (efficient_end - efficient_start)
//    );
}

void random_number_initializer(float *a, int N, bool isZero) {
    float *d_a;
    int chunk = 8192;
    int chunkSize = chunk * chunk * sizeof(float);

    dim3 block(512);
    dim3 grid(chunk * chunk / 512 + 1);
    int rounds = (N/chunk * N/chunk);

    gpuErrorCheck(cudaMalloc((void **) &d_a, chunkSize));
    random_number_gen<<<grid, block>>>(chunk, d_a, isZero);

    for(int i = 0; i < rounds; i++) {
        int startIndex = i * chunk * chunk;
        cudaMemcpy(&a[startIndex], d_a, chunkSize,cudaMemcpyDeviceToHost);
    }
    cudaFree(d_a);

}

void fast_wrapper(float *a, float *b, float *c, int N) {

    float *d_a, *d_b, *d_c, *d_c_intermediate, *d_c_chunk;
    //Lets set max chunk size for fast kernel at 800 x 800.
    int chunk = 800;
    int chunkSize = chunk * chunk * sizeof(float);

    gpuErrorCheck(cudaMalloc((void**) &d_a, chunkSize));
    gpuErrorCheck(cudaMalloc((void**) &d_b, chunkSize));
    gpuErrorCheck(cudaMalloc((void**) &d_c_intermediate, chunkSize * chunk));
    gpuErrorCheck(cudaMalloc((void**) &d_c, chunkSize));
    gpuErrorCheck(cudaMalloc((void**) &d_c_chunk, chunkSize));

    dim3 block(64);
    dim3 grid(chunk * chunk * chunk/64 + 1);
    dim3 block2(32);
    dim3 grid2(chunk * chunk /32 + 1);

    printf("Starting Computation\n");
    for(int i = 0; i < N/chunk * N/chunk; i++) {
        //Load A chunk to the GPU.
        gpuErrorCheck(cudaMemcpy(d_a, &a[i * chunk * chunk], chunkSize, cudaMemcpyHostToDevice));
        int b_row = i % (N/chunk);
        int bRowOffset = b_row * N * chunk;

        int c_row = i / (N/chunk);
        int cRowOffset = c_row * N * chunk;

        for(int j = 0; j < N/chunk; j++) {
//            printf("%d %d\n", i, j);
            gpuErrorCheck(cudaMemcpy(d_b, &b[bRowOffset + (j * chunk * chunk)], chunkSize, cudaMemcpyHostToDevice));
            multiply_kernel<<<grid, block>>>(chunk, d_a, d_b, d_c_intermediate);
            gpuErrorCheck(cudaPeekAtLastError());
            gpuErrorCheck(cudaDeviceSynchronize());
            gpuErrorCheck(cudaMemcpy(d_c, &c[cRowOffset + (j * chunk * chunk)], chunkSize, cudaMemcpyHostToDevice));
            mul_add_kernel<<<grid2, block2>>>(chunk, d_c_intermediate, d_c);
            gpuErrorCheck(cudaPeekAtLastError());
            gpuErrorCheck(cudaDeviceSynchronize());
            gpuErrorCheck(cudaMemcpy(&c[cRowOffset + (j * chunk * chunk)], d_c, chunkSize, cudaMemcpyDeviceToHost));
        }
    }

    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);
    cudaFree(d_c_chunk);
    cudaFree(d_c_intermediate);
}

void fast_wrapper_v2(float *a, float *b, float *c, int N) {

    float *d_a, *d_b, *d_c, *d_c_intermediate, *d_c_chunk;
    //Lets set max chunk size for fast kernel at 800 x 800.
    int chunk = 800;
    int chunkSize = chunk * chunk * sizeof(float);

    gpuErrorCheck(cudaMalloc((void**) &d_a, chunkSize));
    gpuErrorCheck(cudaMalloc((void**) &d_b, chunkSize));
    gpuErrorCheck(cudaMalloc((void**) &d_c_intermediate, chunkSize * chunk));
    gpuErrorCheck(cudaMalloc((void**) &d_c, chunkSize));
    gpuErrorCheck(cudaMalloc((void**) &d_c_chunk, chunkSize));

    dim3 block(64);
    dim3 grid(chunk * chunk * chunk/64 + 1);
    dim3 block2(32);
    dim3 grid2(chunk * chunk + 1);

    printf("Starting Computation\n");
    for(int i = 0; i < N/chunk * N/chunk; i++) {
        //Load A chunk to the GPU.
        gpuErrorCheck(cudaMemcpy(d_a, &a[i * chunk * chunk], chunkSize, cudaMemcpyHostToDevice));
        int b_row = i % (N/chunk);
        int bRowOffset = b_row * N * chunk;

        int c_row = i / (N/chunk);
        int cRowOffset = c_row * N * chunk;

        for(int j = 0; j < N/chunk; j++) {
//            printf("%d %d\n", i, j);
            gpuErrorCheck(cudaMemcpy(d_b, &b[bRowOffset + (j * chunk * chunk)], chunkSize, cudaMemcpyHostToDevice));
            multiply_kernel<<<grid, block>>>(chunk, d_a, d_b, d_c_intermediate);
            gpuErrorCheck(cudaPeekAtLastError());
            gpuErrorCheck(cudaDeviceSynchronize());
            gpuErrorCheck(cudaMemcpy(d_c, &c[cRowOffset + (j * chunk * chunk)], chunkSize, cudaMemcpyHostToDevice));
            mul_add_kernel_ver2<<<grid2, block2>>>(chunk, d_c_intermediate, d_c);
            gpuErrorCheck(cudaPeekAtLastError());
            gpuErrorCheck(cudaDeviceSynchronize());
            gpuErrorCheck(cudaMemcpy(&c[cRowOffset + (j * chunk * chunk)], d_c, chunkSize, cudaMemcpyDeviceToHost));
        }
    }

    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);
    cudaFree(d_c_chunk);
    cudaFree(d_c_intermediate);
}

void efficient_wrapper(float *a, float *b, float *c, int N) {
    int chunk = 8192;
    int chunkSize = chunk * chunk * sizeof(float);

    float *d_a, *d_b, *d_c;

    dim3 block(1024);
    dim3 grid((chunk * chunk)/32);

    gpuErrorCheck(cudaMalloc((void**) &d_a, chunkSize));
    gpuErrorCheck(cudaMalloc((void**) &d_b, chunkSize));
    gpuErrorCheck(cudaMalloc((void**) &d_c, chunkSize));

    printf("Starting Computation\n");
    for(int i = 0; i < N/chunk * N/chunk; i++) {
        //Load A chunk to the GPU.
        gpuErrorCheck(cudaMemcpy(d_a, &a[i * chunk * chunk], chunkSize, cudaMemcpyHostToDevice));
        int b_row = i % (N/chunk);
        int bRowOffset = b_row * N * chunk;

        int c_row = i / (N/chunk);
        int cRowOffset = c_row * N * chunk;

        for(int j = 0; j < N/chunk; j++) {
            printf("%d %d\n", i, j);
            gpuErrorCheck(cudaMemcpy(d_b, &b[bRowOffset + (j * chunk * chunk)], chunkSize, cudaMemcpyHostToDevice));
            gpuErrorCheck(cudaMemcpy(d_c, &c[cRowOffset + (j * chunk * chunk)], chunkSize, cudaMemcpyHostToDevice));
            efficient_multiply<<<grid, block>>>(d_a, d_b, d_c, chunk);
            gpuErrorCheck(cudaPeekAtLastError());
            gpuErrorCheck(cudaMemcpy(&c[cRowOffset + (j * chunk * chunk)], d_c, chunkSize, cudaMemcpyDeviceToHost));
        }
    }
}